# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 22:32:28 2024

@author: USER
"""

from pymongo import errors as mongoerrors, MongoClient
import json
import logging.config
import os
import pandas as pd
import numpy as np
from datetime import datetime
from pandas import json_normalize
import matplotlib.pyplot as plt
import urllib.request
import zipfile
import time
import bson
from scipy.stats import spearmanr, pearsonr
import geopandas as gpd

# %%MANUAL COUNT

# #MERGING ALL FILES TO A SINGLE FIEL WILE INTERPOLATING THE NAN TIMESTAMP
# folder_path = 'Manual_count/Complete'
# files = [f for f in os.listdir(folder_path) if f.endswith('.xlsx')]

# dataframes = []
# for file in files:
#     df = pd.read_excel(os.path.join(folder_path, file))
#     df['timestamp_raw'] = df['timestamp_raw'].fillna(df[
#     'timestamp_raw_back_door'])
#     df['timestamp_raw']= df['timestamp_raw'].interpolate(method='linear',
#     limit_direction='forward')
#     dataframes.append(df)

# merged_df = pd.concat(dataframes)

# output_path = 'Manual_count'
# merged_df.to_excel(os.path.join(output_path, 'merged_data.xlsx'),
# index=False)

# #CHECK NAN VALUES AND FILL THE NAN. CONVERTING TIMESTAMP_RAW TO DATETIME
# AND SAVING AFILE WITH THE CLEAN DATASET
# Check nan values
# manual = pd.read_excel('Manual_count/merged_data.xlsx')
# nan_count = manual.isna().sum()
# ax = nan_count.plot.bar(title='NaN values in manual count dataset')


# manual[['Saliti_front','Discesi_front','Saliti_mid','Discesi_mid',
# 'Saliti_back','Discesi_back','Boarding_manual',
# 'Alighting_manual']]=manual[['Saliti_front','Discesi_front','Saliti_mid',
# 'Discesi_mid', 'Saliti_back','Discesi_back','Boarding_manual',
# 'Alighting_manual']].fillna(0)

# to_drop=['Front stops','Back stops','Fermata effettuata','A bordo_f',
# 'A bordo_b','VL_manual_count','VL_collected_x','VALIDATED?']
# manual=manual.drop(to_drop,axis=1)

# manual=manual.sort_values(by=['Day', 'Linea', 'Corsa','Numero fermata'])
# manual['Daytime(GMT+2or1)']=pd.to_datetime(manual['Daytime(GMT+2or1)'])

# manual['date_timestamp'] = pd.to_datetime(manual['timestamp_raw'],
# unit='ms', utc=True)
# manual['date_timestamp_UTC+'] = pd.to_datetime(manual['timestamp_raw'],
# unit='ms').dt.tz_localize('UTC').dt.tz_convert('Europe/Rome')

# dtype_columns = ['Day', 'Daytime(GMT)','Daytime(GMT+2or1)',
# 'date_timestamp','date_timestamp_UTC+']

# manual[dtype_columns] = manual[dtype_columns].astype(str)

# output_path = 'Manual_count'
# manual.to_excel(os.path.join(output_path, 'merged_data_clean.xlsx'),
# index=False)


# #FILLING THE TERRITORIAL VARIABLES NAN VALUES

# manual = pd.read_excel('Manual_count/merged_data_clean.xlsx')
# to_datetime = ['date_timestamp','date_timestamp_UTC+']
# manual['date_timestamp']=pd.to_datetime(manual['date_timestamp'])
# manual['date_timestamp_UTC+']=pd.to_datetime(manual[
# 'date_timestamp_UTC+'], utc=True).dt.tz_convert('Europe/Rome')

# manual['hour'] = manual['date_timestamp_UTC+'].dt.hour

# to_drop=['vehicle_or_asset_ID', 'Day', 'stop_name', 'Daytime(GMT)',
#        'Daytime(GMT+2or1)', 'date_timestamp', 'date_timestamp_UTC+',
#        'timestamp_raw', 'timestamp_raw_back_door', 'Corsa',
#        'Numero fermata', 'Fermata effettuata.1', 'Saliti_front',
#        'Discesi_front', 'Saliti_mid', 'Discesi_mid', 'Saliti_back',
#        'Discesi_back', 'Boarding_manual', 'Alighting_manual',
#        'VL_calculated',
#        'lat_y', 'lon_x']

# tv=manual.drop(to_drop, axis=1)

# avg_tv=tv.groupby(by=['Linea','Direction','ID_Fermata','hour']).mean()

# tv2=tv.drop(['Linea','Direction', 'Building'], axis=1)
# avg_tv2=tv2.groupby(by=['ID_Fermata','hour']).mean().fillna(0).astype(int)
# avg_tv2.plot.bar()

# build = tv.drop(['Linea','Direction', 'hour', 'Traffic', 'People'], axis=1)
# avg_b=build.groupby(by=['ID_Fermata']).mean().fillna(0).astype(int)

# #Inner join with avg tv 
# manual_in = pd.merge(manual, avg_b, left_on='ID_Fermata', right_index=True)
# manual_in = pd.merge(manual_in, avg_tv2, left_on=['ID_Fermata','hour'],
# right_index=True)

# manual_in['Traffic_x'] = manual_in['Traffic_x'].fillna(manual_in[
# 'Traffic_y'])
# manual_in['People_x'] = manual_in['People_x'].fillna(manual_in['People_y'])
# manual_in['Building_x'] = manual_in['Building_x'].fillna(manual_in[
# 'Building_y'])

# manual_in['lat_y'] = manual_in['lat_y'].fillna(0)
# manual_in['lon_x'] = manual_in['lon_x'].fillna(0)

# nan_count = manual_in.isna().sum()
# ax = nan_count.plot.bar(title='NaN values in manual count dataset')

# manual_in=manual_in.sort_values(by=['Day', 'Linea', 'Corsa','Numero
# fermata'])


# #CLEAN DATA AND SAVING THE LAST CELAN VERSION

# to_drop=['Daytime(GMT)','Daytime(GMT+2or1)', 'timestamp_raw_back_door',
# 'Building_y', 'Traffic_y', 'People_y' ]
# manual_in = manual_in.drop(to_drop, axis=1)


# manual_in['date_timestamp'] = manual_in['date_timestamp'].apply(lambda a:
# datetime.strftime(a,"%Y-%m-%d %H:%M:%S.%f%z"))
# manual_in['date_timestamp_UTC+'] = manual_in['date_timestamp_UTC+'].apply(
# lambda a: datetime.strftime(a,"%Y-%m-%d %H:%M:%S.%f%z"))


# output_path = 'Manual_count'
# manual_in.to_excel(os.path.join(output_path, 'merged_data_clean2.xlsx'),
# index=False)


# ##TEST CLEAN DB
# manual = pd.read_excel('Manual_count/merged_data_clean2.xlsx')
# manual['date_timestamp']=pd.to_datetime(manual['date_timestamp'])
# manual['date_timestamp_UTC+']=pd.to_datetime(manual[
# 'date_timestamp_UTC+'], utc=True).dt.tz_convert('Europe/Rome')

# ## HAY QUE MERGE CON LA UBICACION DE LAS PARADAS#

# stops = pd.read_csv('Manual_count/stops.txt')
# to_drop = ['stop_id','stop_desc', 'zone_id', 'stop_url', 'location_type',
# 'parent_station',
#        'stop_timezone', 'wheelchair_boarding']
# stops=stops.drop(to_drop, axis=1)

# stops['stop_code'] = stops['stop_code'].astype(int)

# manual_s = pd.merge(manual, stops, left_on='ID_Fermata',
# right_on='stop_code')


# manual_s['date_timestamp'] = manual_s['date_timestamp'].apply(lambda a:
# datetime.strftime(a,"%Y-%m-%d %H:%M:%S.%f%z"))
# manual_s['date_timestamp_UTC+'] = manual_s['date_timestamp_UTC+'].apply(
# lambda a: datetime.strftime(a,"%Y-%m-%d %H:%M:%S.%f%z"))


# output_path = 'Manual_count'
# manual_s.to_excel(os.path.join(output_path,
# 'merged_data_clean2_stops.xlsx'), index=False)


# OPEN READY MC DATA
manual = pd.read_excel('Manual_count/merged_data_clean2.xlsx')
manual['date_timestamp'] = pd.to_datetime(manual['date_timestamp'])
manual['date_timestamp_UTC+'] = pd.to_datetime(manual['date_timestamp_UTC+'],
                                               utc=True).dt.tz_convert(
    'Europe/Rome')

# FILTERING JUNE
manual_filt = manual.loc[(manual["date_timestamp_UTC+"] >= '2023-06-03')]
manual_filt['date_timestamp_UTC+'] = manual_filt['date_timestamp_UTC+'].apply(
    lambda a: datetime.strftime(a, "%Y-%m-%d %H:%M:%S.%f%z"))
manual_filt['date_timestamp'] = manual_filt['date_timestamp'].apply(
    lambda a: datetime.strftime(a, "%Y-%m-%d %H:%M:%S.%f%z"))

output_path = 'Manual_count'
manual_filt.to_excel(
    os.path.join(output_path, 'merged_data_clean2_stops_June.xlsx'),
    index=False)

# %%Cleaning GTT- DONE

# merging all files in one
# folder_path = 'GTT_APC/Completos'
# files = [f for f in os.listdir(folder_path) if f.endswith('.xlsx')]

# dataframes = []
# for file in files:
#     df = pd.read_excel(os.path.join(folder_path, file))
#     # df['timestamp_raw'] = df['timestamp_raw'].fillna(df[
#     'timestamp_raw_back_door'])
#     df['Data e ora']= df['Data e ora'].interpolate(method='linear',
#     limit_direction='forward')
#     dataframes.append(df)

# merged_df = pd.concat(dataframes)

# output_path = 'GTT_APC'
# merged_df.to_excel(os.path.join(output_path, 'merged_data_GTT.xlsx'),
# index=False)


# gtt = pd.read_excel('GTT_APC/merged_data_GTT.xlsx')

# gtt['Data e ora']=pd.to_datetime(gtt['Data e ora'], format= '%d/%m/%Y
# %H:%M:%S')
# nan_count = gtt.isna().sum()
# ax = nan_count.plot.bar(title='NaN values in GTT APC dataset')

# to_drop=['Codice autista', 'Nome autista', 'Cognome autista', 'Tipo veicolo',
#         'Codice deposito di apparteneza',
#         'Descrizione deposito di appartenenza', 'Corsa',       
#         'Eccessivo affollamento']

# gtt=gtt.drop(to_drop, axis=1)

# gtt['Tipo di stop']=gtt['Tipo di stop'].fillna(0)     

# gtt = gtt.dropna(subset=['Linea'])
# gtt['Data e ora']= gtt['Data e ora'].interpolate(method='linear',
# limit_direction='forward')
# gtt=gtt.sort_values(by=['Data e ora'])
# gtt = gtt.dropna(subset=['Codice nodo'])

# output_path = 'GTT_APC'
# gtt.to_excel(os.path.join(output_path, 'merged_data_GTT_clean.xlsx'),
# index=False)

# %%
# %%WIFI APC

wifi = pd.read_csv('WifiAPC/Wi_Fi_APC_COUNT_03_Jun_2023_01_July_2023.csv')

wifi['properties'] = wifi['properties'].map(eval)

wifi2 = pd.json_normalize(wifi['properties'], max_level=1)
wifi3 = pd.json_normalize(wifi, max_level=1)

# %%MC + GTT

manual = pd.read_excel('Manual_count/merged_data_clean2_stops_June.xlsx')
manual['date_timestamp'] = pd.to_datetime(manual['date_timestamp'])
manual['date_timestamp_UTC+'] = pd.to_datetime(manual['date_timestamp_UTC+'],
                                               utc=True).dt.tz_convert(
    'Europe/Rome')
manual = manual.sort_values(by=['date_timestamp_UTC+'])

gtt = pd.read_excel('GTT_APC/merged_data_GTT_clean.xlsx')
gtt['Data e ora'] = pd.to_datetime(gtt['Data e ora'],
                                   format='%d/%m/%Y %H:%M:%S').dt.tz_localize(
    'Europe/Rome')

manual_gtt = pd.merge_asof(manual, gtt, left_on="date_timestamp_UTC+",
                           right_on='Data e ora', by=['Linea', 'ID_Fermata'],
                           direction='nearest', tolerance=pd.Timedelta("5m"))
nan_count = manual_gtt.isna().sum()
ax = nan_count.plot.bar(title='NaN values in manual count dataset')

manual_gtt['time_diff'] = (manual_gtt['date_timestamp_UTC+'] - manual_gtt[
    'Data e ora']) / pd.Timedelta(seconds=1)
manual_gtt['Data e ora'] = manual_gtt['Data e ora'].fillna(
        manual_gtt['date_timestamp_UTC+'])  # fill nan with other column

to_rename = {'stop_name_x': 'stop_name',
             'date_timestamp': 'date_timestamp_MC',
             'date_timestamp_UTC+': 'date_timestamp_UTC+_MC',
             'timestamp_raw': 'timestamp_raw_MC',
             'Fermata effettuata.1': 'Fermata effettuata',
             'Saliti_front': 'Boarding_front_MC',
             'Discesi_front': 'Alighting_front_MC',
             'Saliti_mid': 'Boarding_mid_MC',
             'Discesi_mid': 'Alighting_mid_MC',
             'Saliti_back': 'Boarding_back_MC',
             'Discesi_back': 'Alighting_back_MC',
             'Boarding_manual': 'Total_boarding_MC',
             'Alighting_manual': 'Total_alighting_MC',
             'VL_calculated': 'VL_MC', 'Traffic_x': 'Traffic',
             'People_x': 'People', 'Building_x': 'Building', 'lat_y': 'lat_MC',
             'lon_x': 'lon_MC', 'Data e ora': 'date_timestamp_GTT',
             'Passeggeri presenti': 'VL_GTT',
             'Totale saliti': 'Total_boarding_GTT',
             'Totale scesi': 'Total_alighting_GTT',
             'Saliti porta 1': 'Boarding_front_GTT',
             'Scesi porta 1': 'Alighting_front_GTT',
             'Saliti porta 2': 'Boarding_mid_GTT',
             'Scesi porta 2': 'Alighting_mid_GTT',
             'Saliti porta 3': 'Boarding_back_GTT',
             'Scesi porta 3': 'Alighting_back_GTT'}

manual_gtt.rename(columns=to_rename, inplace=True)

to_drop = ['stop_code', 'stop_name_y', 'Descrizione nodo', 'Saliti porta 4',
           'Scesi porta 4', 'Tipo di stop']
manual_gtt = manual_gtt.drop(to_drop, axis=1)

# Convert dateimte to text before saviong to excel
datetime_variables = ['date_timestamp_MC', 'date_timestamp_UTC+_MC',
                      'date_timestamp_GTT']

for i in datetime_variables:
    print(i)
    manual_gtt[i] = manual_gtt[i].apply(
        lambda a: datetime.strftime(a, "%Y-%m-%d %H:%M:%S.%f%z"))

output_path = 'Merging datasets'
manual_gtt.to_excel(os.path.join(output_path, 'manual_gtt_clean.xlsx'),
                    index=False)

# Convert dateimte to text before saviong to excel
datetime_variables = ['date_timestamp', 'date_timestamp_UTC+', 'Data e ora']

for i in datetime_variables:

    manual_gtt[i] = manual_gtt[i].apply(
        lambda a: datetime.strftime(a, "%Y-%m-%d %H:%M:%S.%f%z"))

output_path = 'Merging datasets'
manual_gtt.to_excel(os.path.join(output_path, 'manual_gtt.xlsx'), index=False)

# ANALYSIS NAN PER DAYS

# #Nan values per day
# df = manual_gtt.copy()
# df['count_nan'] = df['Passeggeri presenti'].isnull()
# df['count_nan'] = df.groupby(['Day','hour'])['count_nan'].transform(
# 'sum').astype(int)


# df['Data e ora'] = df['Data e ora'].fillna(df['date_timestamp_UTC+'])
# #fill nan with other column
# #Convert dateimte to text before saviong to excel
# datetime_variables=['date_timestamp','date_timestamp_UTC+', 'Data e ora']

# for i in datetime_variables:

#     df[i] = df[i].apply(lambda a: datetime.strftime(a,"%Y-%m-%d
#     %H:%M:%S.%f%z"))

# output_path = 'Merging datasets'
# df.to_excel(os.path.join(output_path, 'manual_gtt_nan_day2.xlsx'),
# index=False)

# # ax = manual_gtt['time_diff'].plot.bar(title='Timestamp differences
# manual count vs. GTT')

# %%Pollution


# ##MERGING ALL FILES TO A SINGLE FIEL WILE INTERPOLATING THE NAN TIMESTAMP
# folder_path = 'Pollution/Stations_data'
# files = [f for f in os.listdir(folder_path) if f.endswith('.csv')]

# dataframes = []
# for file in files:
#     print(str(file))
#     df = pd.read_csv(os.path.join(folder_path, file), on_bad_lines='warn',
#     sep=';')
#     # df['timestamp_raw'] = df['timestamp_raw'].fillna(df[
#     'timestamp_raw_back_door'])
#     # df['timestamp_raw']= df['timestamp_raw'].interpolate(
#     method='linear', limit_direction='forward')
#     dataframes.append(df)

# merged_df = pd.concat(dataframes).reset_index()

# output_path = 'Pollution'
# merged_df.to_excel(os.path.join(output_path, 'merged_data.xlsx'),
# index=False)
s_info = pd.read_excel('Pollution/StationsInfo/Stations_compilato.xlsx')
s = s_info['Station'].str.replace(" ", "")

# pol = pd.read_excel('Pollution/merged_data.xlsx')
# pol = pol.dropna(subset=['Descr. Un. misura'])

# to_drop=['Ora', 'Id Rete Monitoraggio','Codice Istat Comune',
# 'Denominazione Stazione',
#        'Descr. Parametro', 'Id Un. misura',
#        'Valore', 'Stato', 'Data rilevamento']

# pol = pol.drop(to_drop, axis=1)

# Remove empty spaces in str
# pol['Progr. Punto Comune']=pol['Progr. Punto Comune'].str.replace(" ", "")
# pol['Id Parametro']=pol['Id Parametro'].str.replace(" ", "")

# pol_day=pol.groupby(by=['index', 'Progr. Punto Comune',
# 'Id Parametro']).mean()
# pol_day = pol_day.reset_index()
# pol_day.to_excel(os.path.join(output_path, 'merged_data_day.xlsx'),
# index=False)


# pol_day2 = pol_day.drop('index', axis=1)


# I manually checked on excel the common Id parametro that ie measured by
# all the sensors.
# table = pd.pivot_table(pol_day2, values=['Progr. Punto Comune',
# 'Id Parametro'], index=['Progr. Punto Comune', 'Id Parametro'],aggfunc={
# 'Descr. Un. misura': "mean"})
# table = table.reset_index()
# table.to_excel(os.path.join(output_path, 'dist_values_stations.xlsx'),
# index=False)

# u_para = pol_day['Id Parametro'].unique()


# s_cons = pol_day[pol_day['Progr. Punto Comune'] == s[0]]
# u_cons= s_cons['Id Parametro'].unique()

# s_grassi = pol_day[pol_day['Progr. Punto Comune'] == s[1]]
# u_grassi= s_grassi['Id Parametro'].unique()

# s_lingo = pol_day[pol_day['Progr. Punto Comune'] == s[2]]
# u_lingo= s_lingo['Id Parametro'].unique()

# s_reba = pol_day[pol_day['Progr. Punto Comune'] == s[3]]
# u_reba= s_reba['Id Parametro'].unique()

# s_rubi = pol_day[pol_day['Progr. Punto Comune'] == s[4]]
# u_rubi= s_rubi['Id Parametro'].unique()


pol = pd.read_excel('Pollution/merged_data_day.xlsx')

pol_coord = pd.merge(pol, s_info, left_on='Progr. Punto Comune',
                     right_on="Station")
u_param = pol_coord['Id Parametro'].unique()

# output_path = 'Pollution'
# pol_coord_PM10.to_excel(os.path.join(output_path, 'PM10_coord.xlsx'),
# index=False)


pol_coord_PM10 = pol_coord[pol_coord['Id Parametro'] == 'PM10-BassoVolume']
pol_coord_PM10['pair'] = list(
    zip(pol_coord_PM10['index'], pol_coord_PM10['Descr. Un. misura']))

dates = ['03/06/2023', '06/06/2023', '07/06/2023', '09/06/2023', '12/06/2023',
         '13/06/2023', '19/06/2023',
         '20/06/2023', '21/06/2023', '24/06/2023', '27/06/2023', '29/06/2023',
         '30/06/2023', '01/07/2023']
dataframes = []
for date in dates:
    print(str(date))
    df = pol_coord_PM10[pol_coord_PM10['index'] == date]
    new_string = date.replace("/", "_")
    df.to_excel(os.path.join(output_path, 'PM10_' + str(new_string) + '.xlsx'),
                index=False)
    # df['timestamp_raw'] = df['timestamp_raw'].fillna(df[
    # 'timestamp_raw_back_door'])
    # df['timestamp_raw']= df['timestamp_raw'].interpolate(method='linear',
    # limit_direction='forward')
    dataframes.append(df)

PM10_dates = pd.concat(dataframes).reset_index()

output_path = 'Pollution'
PM10_dates.to_excel(os.path.join(output_path, 'PM10_alldates.xlsx'),
                    index=False)

# %%#MERGE WIFI APC WITH MANUAL_GTT_CLEAN AND THEN CONVERT IT TO GEOJSON (OR
gtt_mc = pd.read_excel('Merging datasets/manual_gtt_clean.xlsx')

df_WiFi_APC_mc = pd.read_excel(
        "WifiAPC/WiFi_APC_and_Manual_count_June_2023_with_XGB.xlsx")

df_WiFi_APC_mc['timestamp_dt_main'] = df_WiFi_APC_mc['timestamp_dt_mc']*1000

df_WiFi_APC_mc_select_cols = df_WiFi_APC_mc.copy()

df_WiFi_APC_mc_select_cols['WiFi_predicted_count'] = df_WiFi_APC_mc_select_cols['predicted_count']
df_WiFi_APC_mc_select_cols['hour'] = df_WiFi_APC_mc_select_cols['Hour']
df_WiFi_APC_mc_select_cols = df_WiFi_APC_mc_select_cols[['timestamp_dt_main',
'ID_Fermata', 'Next stop ID', 'Day', 'stop_name',
'Linea', 'Corsa', 'Numero fermata',
'Fermata effettuata', 'hour', 'Minute',
'occupants_count', 'WiFi_predicted_count']]

gtt_mc.sort_values("timestamp_raw_MC",inplace=True)
df_WiFi_APC_mc_select_cols.sort_values(
                                  "timestamp_dt_main",inplace=True)

df_WiFi_APC_mc_select_cols['timestamp_dt_main'] = df_WiFi_APC_mc_select_cols[
    'timestamp_dt_main'].astype('float64')

gtt_mc['hour'] = gtt_mc[
    'hour'].astype('Int64')

df_WiFi_APC_mc_select_cols['hour'] = df_WiFi_APC_mc_select_cols[
    'hour'].astype('Int64')

gtt_mc['Linea'] = gtt_mc[
    'Linea'].astype('str')

df_WiFi_APC_mc_select_cols['Linea'] = df_WiFi_APC_mc_select_cols[
    'Linea'].astype('str')

gtt_mc['Corsa'] = gtt_mc[
    'Corsa'].astype('Int64')

df_WiFi_APC_mc_select_cols['Corsa'] = df_WiFi_APC_mc_select_cols[
    'Corsa'].astype('Int64')

df_merged = pd.merge_asof(gtt_mc,
                          df_WiFi_APC_mc_select_cols,
                          # how="inner",
                          left_on="timestamp_raw_MC",
                          right_on="timestamp_dt_main",
                          by=["Linea","Corsa","hour"],
                          # Ensures that these columns match exactly in the
                          # range-based merge.
                          direction="forward",
                          tolerance=3000000)
df_merged.to_excel("Merging datasets/WiFi_GTT_and_Manual_count_June_2023"
                   ".xlsx")


# DataFrame where rows with NaN in "VL_GTT" are dropped
df_vl_gtt_clean = df_merged.dropna(subset=['VL_GTT'])

# DataFrame where rows with NaN in "WiFi_predicted_count" are dropped
df_wifi_predicted_clean = df_merged.dropna(subset=['WiFi_predicted_count'])




# Creating a new DataFrame where NaN values in specified columns are replaced with 0
df_merged_filled = df_merged.fillna({'VL_GTT': 0, 'WiFi_predicted_count': 0,
                                     'occupants_count': 0})
df_wifi_predicted_clean = df_wifi_predicted_clean.fillna({'occupants_count': 0})
df_vl_gtt_clean = df_vl_gtt_clean.fillna({'occupants_count': 0})


df_merged_filled.to_excel("Merging datasets/FILLED_WiFi"
                                 "_GTT_and_Manual_count_June_2023"
                   ".xlsx")
df_vl_gtt_clean.to_excel("Merging datasets/WiFi_GTT(CLEAN)"
                         "_and_Manual_count_June_2023"
                   ".xlsx")
df_wifi_predicted_clean.to_excel("Merging datasets/WiFi(CLEAN)"
                                 "_GTT_and_Manual_count_June_2023"
                   ".xlsx")




def corr_func(Actual, Forecasted):
    """

    returns spearman and pearson correlation coefficeints
    """

    pearson_corr, pearson_corr_prob = pearsonr(Actual, Forecasted)
    spearman_corr, spearman_corr_p_val = spearmanr(Actual, Forecasted)

    return pearson_corr, pearson_corr_prob, spearman_corr, spearman_corr_p_val


def accuracy_func(Actual, Forecasted, CONF_INTERV=0, ALWAYS_PRESENT_DEV=0):
    """

    :type ALWAYS_PRESENT_DEV: object
    """
    Forecasted = Forecasted.apply(
            lambda x: (
                    x - ALWAYS_PRESENT_DEV) if x > ALWAYS_PRESENT_DEV
            else abs(
                    x))

    NUMERATOR_VALUE = np.abs(Forecasted - Actual)

    rmse = np.sqrt(np.mean(np.square(Forecasted - Actual)))

    return (100 / len(Actual)) * np.sum(NUMERATOR_VALUE / (
            np.abs(Actual) + np.abs(Forecasted))), round(np.mean(
            NUMERATOR_VALUE), 2),rmse


def check_correlation_and_accuracy(y_real, y_pred,
                                   ci, apd, rssi_val, manual_adjust=2,
                                   model_val='simple'):
    global accuracy_df
    smape_val, mae_val,rmse = accuracy_func(y_real, y_pred, ci, apd)
    pearson_corr, pearson_p_val, spearman_corr, spearman_p_val = corr_func(
            y_real,
            y_pred)

    # only makes sense when mape < 1
    percentage_accuracy = (100 - smape_val)
    print("##################################################")

    print("percentage_accuracy:",
          percentage_accuracy)
    print("Mean absolute error:", mae_val)
    print("RMSE:", rmse)
    print("###### Correlation Co-efficents ########")
    print("PearsonR(1-Perfect pos linear rel, 0- no linear rel, "
          "-1 - perfect neg linear rel):", pearson_corr, pearson_p_val)
    print("SpearmanRho:", spearman_corr, spearman_p_val)
    # print("KendallTau:", KendallTau)
    df2 = pd.DataFrame([[model_val, rssi_val, manual_adjust,
                         percentage_accuracy, mae_val,
                         pearson_corr, pearson_p_val, spearman_corr,
                         spearman_p_val]],
                       columns=['model', 'rssi_val', 'manual_adjust',
                                'percentage_accuracy', 'Mean abs error',
                                'pearson_corr', 'pearson_p_val',
                                'spearman_corr', 'spearman_p_val'])

    accuracy_df = pd.concat([accuracy_df, df2], ignore_index=True, axis=0)
    print("##################################################")
    return df2


def plot_corr(rs, acc_val, seconds_per_row):
    corr_offset = np.ceil(len(rs) / 2) - np.argmax(rs)
    corr_offset_secs = corr_offset * seconds_per_row
    acc_offset = (np.ceil(len(acc_val) / 2) - np.argmax(acc_val)) * \
                 seconds_per_row
    f, ax = plt.subplots(figsize=(14, 10))

    ax2 = ax.twinx()
    ax2.plot(acc_val, 'b-')
    ax2.set_ylabel('Accuracy - SMAPE', color='b')

    ax.plot(rs, 'r-')
    ax.plot(acc_val)
    ax.set_ylabel('Pearson r', color='r')
    ax.axvline(np.ceil(len(rs) / 2), color='k', linestyle='--', label='Center')
    ax.axvline(np.argmax(rs), color='r', linestyle='--',
               label='Peak synchrony')
    ax.axvline(np.argmax(acc_val), color='b', linestyle='--',
               label='Peak accuracy')
    print(max(rs))
    print(np.argmax(rs))
    ax.set(title=f'Time lagged cross correlation  '
                 f'\n Corr_Offset_secs = {corr_offset_secs} -'
                 f'  Max corr = {max(rs)}'
                 f'\n Acc_Offset_secs = {acc_offset} seconds -'
                 f' Max acc = {max(acc_val)}\n'
                 f'Manual leads <> APC leads',
           ylim=[.1, 1.01], xlim=[0, len(rs)], xlabel='Offset(1x=10 secs)')
    ax.set_xticklabels(
            [int(item - np.ceil(len(rs) / 2)) for item in ax.get_xticks()])
    plt.legend()
    cwd = os.getcwd()

    os.makedirs(f'{cwd}\\Images\\', exist_ok=True)
    f.savefig(
            f'{cwd}\\Images\\Image_corr.png')  # save
    # the figure to
    # file
    plt.close(f)


def plot_real_vs_pred(X_org_combined, model='RF', model_setting='n_est_1000'):

    # gca stands for 'get current axis'

    rslt_df = X_org_combined.copy()
    for title, group in rslt_df.groupby('Day'):
        ax = plt.gca()
        # print(group.shape)
        group.plot(kind='line', x='datetime_mc', y='occupants_count', ax=ax)
        group.plot(kind='line', x='datetime_mc',
                   y=f'predicted_count', color='red',
                   ax=ax)

        cwd = os.getcwd()
        os.makedirs(f'{cwd}\\Images\\PRED_vs_Real\\Model_RSSI_MAC\\{model}'
                    , exist_ok=True)
        plt.savefig(
                f'{cwd}\\Images\\PRED_vs_Real\\Model_RSSI_MAC\\{model}\\'
                f'40_Days_RSSI_analysis_{model}_{model_setting}_{title}.png')
        # save
        # the figure to
        # file
        plt.close()


accuracy_df = pd.DataFrame()

print("#################### Checking Filled WiFi ###############")
check_correlation_and_accuracy(df_merged_filled['occupants_count'],
                               df_merged_filled[
                                   f'WiFi_predicted_count'],
                               ci=0,
                               apd=0,
                               rssi_val=f"WiFi_XGB",
                               manual_adjust=0,
                               model_val=f"WiFi_System")

print("#################### Checking Filled GTT ###############")
check_correlation_and_accuracy(df_merged_filled['occupants_count'],
                               df_merged_filled[
                                   f'VL_GTT'],
                               ci=0,
                               apd=0,
                               rssi_val=f"WiFi_XGB",
                               manual_adjust=0,
                               model_val=f"GTT_System")

print("#################### Checking Clean WiFi ###############")
check_correlation_and_accuracy(df_wifi_predicted_clean['occupants_count'],
                               df_wifi_predicted_clean[
                                   f'WiFi_predicted_count'],
                               ci=0,
                               apd=0,
                               rssi_val=f"WiFi_XGB",
                               manual_adjust=0,
                               model_val=f"WiFi_System")

print("#################### Checking Clean GTT ###############")
check_correlation_and_accuracy(df_vl_gtt_clean['occupants_count'],
                               df_vl_gtt_clean[
                                   f'VL_GTT'],
                               ci=0,
                               apd=0,
                               rssi_val=f"WiFi_XGB",
                               manual_adjust=0,
                               model_val=f"GTT_System")

# %% end of Merging passenger count datasets

#%% POLLUTION DATA AFTER ARCGIS


#read Shp file for pollution

pollution_df = gpd.read_file('Pollution/PM10 data/PM10_day.shp')
pollution_df.head()
pollution_df.to_crs(crs=4326,inplace=True)

# Converting date from column label to row value
pollution_df_reorg = pd.melt(pollution_df,
              id_vars=['geometry','Layer','OBJECTID','Join_Count',
                       'TARGET_FID','PM10_anual'],
              value_vars=['v_01_07', 'v_03_06', 'v_06_06', 'v_07_06', 'v_09_06', 'v_12_06',
       'v_13_06', 'v_19_06', 'v_20_06', 'v_21_06', 'v_29_06', 'v_30_06'])

# Extracting date and month, and constructing a full date string with the year 2023
pollution_df_reorg['new_date'] = pollution_df_reorg['variable'].str.split(
        '_').str[1] + '-' + pollution_df_reorg['variable'].str.split(
        '_').str[2] + '-2023'

# Converting the string to a datetime format
pollution_df_reorg['new_date'] = pd.to_datetime(pollution_df_reorg[
                                                    'new_date'],
                                                format='%d-%m-%Y')
pollution_df_reorg_sel = pollution_df_reorg[['geometry','value', 'new_date']]

from shapely.geometry import Point

df_merged_filled['Day_x'] = pd.to_datetime(df_merged_filled['Day_x'])
# Step 1: Convert df_merged_filled to a GeoDataFrame
df_merged_filled['geometry'] = df_merged_filled.apply(lambda row: Point(row['stop_lon'], row['stop_lat']), axis=1)
df_merged_filled_gpd = gpd.GeoDataFrame(df_merged_filled, geometry='geometry', crs="EPSG:4326")



df_temp = gpd.sjoin_nearest(df_merged_filled_gpd, pollution_df_reorg_sel,
                         how="inner",
                             rsuffix='_pollution',
                             distance_col="stop_poll_station_dist")

df_filtered = df_temp[df_temp['Day_x'] == df_temp['new_date']]

# Step 3: Remove additional matches by sorting by 'stop_poll_station_dist' and dropping duplicates
df_filtered = df_filtered.sort_values(
        by='stop_poll_station_dist').drop_duplicates(subset=['Day_x',
                                                             'ID_Fermata_x',
                                                             'Linea','Corsa'
                                                             ],
                                                             keep='first')

# Now df_filtered contains only the closest match from pollution_df_reorg_sel for each row in df_merged_filled_gpd
final_df = df_filtered

final_df = final_df[['vehicle_or_asset_ID', 'Day_x', 'ID_Fermata_x', 'stop_name_x',
       'date_timestamp_MC', 'date_timestamp_UTC+_MC', 'timestamp_raw_MC',
       'Linea', 'Corsa', 'Direction', 'Numero fermata_x',
       'Fermata effettuata_x', 'Boarding_front_MC', 'Alighting_front_MC',
       'Boarding_mid_MC', 'Alighting_mid_MC', 'Boarding_back_MC',
       'Alighting_back_MC', 'Total_boarding_MC', 'Total_alighting_MC', 'VL_MC',
       'Traffic', 'People', 'Building', 'lat_MC', 'lon_MC', 'hour', 'stop_lat',
       'stop_lon', 'date_timestamp_GTT', 'Mezzo', 'VL_GTT',
       'Total_boarding_GTT', 'Total_alighting_GTT', 'Boarding_front_GTT',
       'Alighting_front_GTT', 'Saliti porta 2 ', 'Alighting_mid_GTT',
       'Boarding_back_GTT', 'Alighting_back_GTT', 'time_diff',
       'timestamp_dt_main', 'ID_Fermata_y', 'Next stop ID', 'Day_y',
       'stop_name_y', 'Numero fermata_y', 'Fermata effettuata_y', 'Minute',
       'occupants_count', 'WiFi_predicted_count', 'geometry',
                     'value']].rename(columns={'value': 'value_pollution_PM10'})

final_df.to_excel("Merging "
                  "datasets/Pollution_WiFi_GTT_and_Manual_count_June_2023"
                   ".xlsx")
#%%METEO DATA 


carpetas = ['Precipitazione','Radiazione','Temperatura aria', 'Umidita','Velocita vento']
path = 'Meteo/'

for carpeta in carpetas:
    folder_path=path+str(carpeta)
    print(folder_path)

    files = [f for f in os.listdir(folder_path) if f.endswith('.xlsx')]
    # print(files)

    dataframes = []
    for file in files:
        df = pd.read_excel(os.path.join(folder_path, file),sheet_name=1)
        df = df.dropna()
        df['valore']=pd.to_numeric(df['valore'],errors='coerce',downcast='float')
        df['valore']=df['valore'].ffill()
        to_drop = ['flag validazione']
        df = df.drop(to_drop, axis=1)
        
        
        # df['timestamp_raw'] = df['timestamp_raw'].fillna(df[
        # 'timestamp_raw_back_door'])
        # df['timestamp_raw']= df['timestamp_raw'].interpolate(method='linear',
        # limit_direction='forward')
        dataframes.append(df)

    merged_df = pd.concat(dataframes)
    u= merged_df['parametro'].dropna().unique()[0]
    print(u)
    merged_df['parametro'] =merged_df['parametro'].fillna(u)
    output_path = 'Meteo'
    merged_df.to_excel(os.path.join(output_path, 'merged_data_'+str(carpeta)+'.xlsx'), index=False)


prec = pd.read_excel('Meteo/merged_data_Precipitazione.xlsx')
radi =pd.read_excel('Meteo/merged_data_Radiazione.xlsx')
ta = pd.read_excel('Meteo/merged_data_Temperatura aria.xlsx')
umi=pd.read_excel('Meteo/merged_data_Umidita.xlsx')
velo=pd.read_excel('Meteo/merged_data_Velocita vento.xlsx')



prec_h=prec.groupby(by=['data', 'ora (UTC)']).agg({'sensor': 'unique',  'parametro':'unique', 'valore': 'mean'})
prec_h.rename(columns = {'sensor': 'sensor_prec', 'parametro':'parametro_prec', 'valore':'valore_prec'}, inplace = True)

radi_h = radi.groupby(by=['data', 'ora (UTC)']).agg({'sensor': 'unique',  'parametro':'unique', 'valore': 'mean'})
radi_h.rename(columns = {'sensor': 'sensor_radi', 'parametro':'parametro_radi', 'valore':'valore_radi'}, inplace = True)

ta_h = ta.groupby(by=['data', 'ora (UTC)']).agg({'sensor': 'unique',  'parametro':'unique', 'valore': 'mean'})
ta_h.rename(columns = {'sensor': 'sensor_temp_aria', 'parametro':'parametro_temp_aria', 'valore':'valore_temp_aria'}, inplace = True)

umi_h = umi.groupby(by=['data', 'ora (UTC)']).agg({'sensor': 'unique',  'parametro':'unique', 'valore': 'mean'})
umi_h.rename(columns = {'sensor': 'sensor_hum', 'parametro':'parametro_hum', 'valore':'valore_hum'}, inplace = True)

velo_h = velo.groupby(by=['data', 'ora (UTC)']).agg({'sensor': 'unique',  'parametro':'unique', 'valore': 'mean'})
velo_h.rename(columns = {'sensor': 'sensor_wind_speed', 'parametro':'parametro_wind_speed', 'valore':'valore_wind_speed'}, inplace = True)


all_meteo = ta_h.join([prec_h,umi_h,radi_h,velo_h]).reset_index()
all_meteo.to_excel(os.path.join(output_path, 'merged_data_all_meteo.xlsx'), index=False)


#%%Merging all mneteo with Pollution_WiFi_GTT_and_Manual_count_June_2023.xlsx


df = pd.read_excel('Merging datasets/4.Pollution_WiFi_GTT_and_Manual_count_June_2023.xlsx')
df['date_timestamp_UTC+_MC'] = pd.to_datetime(df['date_timestamp_UTC+_MC'], utc=True).dt.tz_convert('Europe/Rome')
df['Day_x'] = df['date_timestamp_UTC+_MC'].dt.date

# all_meteo = pd.read_excel('Meteo/merged_data_all_meteo.xlsx')


# # all_meteo['data'] = pd.to_datetime(all_meteo['data'], format='%Y-%m-%d')
# # all_meteo['ora (UTC)+'] =  pd.to_datetime(all_meteo['ora (UTC)'], format='%H:%M:%S').dt.tz_localize('UTC')

# all_meteo['data2'] = all_meteo['data'].astype(str).str.split(' ', expand=True)
# all_meteo[['data3', 'fake ora']]= all_meteo['data2'].apply(pd.Series)

# all_meteo['data4']= all_meteo['data3']+' '+all_meteo['ora (UTC)']
# all_meteo['fake ora']=all_meteo['fake ora'].fillna(1)
                                                                       
                
# test =all_meteo.loc[all_meteo['fake ora']==1]  
# test2 = all_meteo.loc[all_meteo['fake ora'] != 1]    
                                      
# all_meteo['ora (UTC)+_1']= pd.to_datetime(test['data4'], dayfirst=True, format='%d/%m/%Y %H:%M:%S').dt.tz_localize('UTC').dt.tz_convert('Europe/Rome')                                                        
# all_meteo['ora (UTC)+']= pd.to_datetime(test2['data4']).dt.tz_localize('UTC').dt.tz_convert('Europe/Rome')                                                        
                                                            
# all_meteo['ora (UTC)+'] = all_meteo['ora (UTC)+'].fillna(all_meteo['ora (UTC)+_1'])                                                             
                                                            
# to_drop = ['data', 'ora (UTC)', 'sensor_temp_aria', 'parametro_temp_aria',
#        'sensor_prec', 'parametro_prec', 
#        'sensor_hum', 'parametro_hum', 'sensor_radi',
#        'parametro_radi', 'sensor_wind_speed',
#        'parametro_wind_speed', 'data2', 'data3',
#        'fake ora', 'data4',  'ora (UTC)+_1']                                                            
                                                             
# all_meteo = all_meteo.drop(to_drop, axis=1)                                                             
# all_meteo['ora (UTC)+'] = all_meteo['ora (UTC)+'].apply(lambda a: datetime.strftime(a,"%Y-%m-%d %H:%M:%S%z"))                                                             
# output_path = 'Meteo'
# all_meteo.to_excel(os.path.join(output_path, 'merged_data_all_meteo_CLEAN.xlsx'), index=False)                                                             
                                                             
                                                             
all_meteo = pd.read_excel('Meteo/merged_data_all_meteo_CLEAN.xlsx')                                                             
all_meteo['ora (UTC)+'] = pd.to_datetime(all_meteo['ora (UTC)+'], utc=True).dt.tz_convert('Europe/Rome')
all_meteo['date'] =  all_meteo['ora (UTC)+'].dt.date  
all_meteo['hour'] =  all_meteo['ora (UTC)+'].dt.hour                                               
                                                             
df_meteo = df.merge(all_meteo, left_on=['Day_x','hour'], right_on=['date','hour'])                                                             
df_meteo = df_meteo.drop(['Unnamed: 0','ora (UTC)+', 'date'], axis=1)



df_meteo['date_timestamp_UTC+_MC'] = df_meteo['date_timestamp_UTC+_MC'].apply(lambda a: datetime.strftime(a,"%Y-%m-%d %H:%M:%S.%f%z"))                                                             
output_path = 'Merging datasets'
df_meteo.to_excel(os.path.join(output_path, '5.Meteo_Pollution_WiFi_GTT_and_Manual_count_June_2023.xlsx'), index=False)                                                         
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             





#%%Territorial variables - population after GIS

#read Shp file for territorial variables
pop_df = gpd.read_file('Territorial information/centroid_pop_statzones/centroids_pop_statzones.shp')
pop_df.head()
pop_df.to_crs(crs=4326,inplace=True)


df = pd.read_excel('Merging datasets/5.Meteo_Pollution_WiFi_GTT_and_Manual_count_June_2023.xlsx')
df['date_timestamp_UTC+_MC'] = pd.to_datetime(df['date_timestamp_UTC+_MC'], utc=True).dt.tz_convert('Europe/Rome')
df['Day_x'] = df['date_timestamp_UTC+_MC'].dt.date


from shapely.geometry import Point


# Step 1: Convert df_merged_filled to a GeoDataFrame
df['geometry'] = df.apply(lambda row: Point(row['stop_lon'], row['stop_lat']), axis=1)
df_gpd = gpd.GeoDataFrame(df, geometry='geometry', crs="EPSG:4326")



df_temp = gpd.sjoin_nearest(df_gpd, pop_df,
                         how="inner",
                             rsuffix='_popu',
                             distance_col="stop_population_station_dist")


# Step 3: Remove additional matches by sorting by 'stop_poll_station_dist' and dropping duplicates
df_filtered = df_temp.sort_values(
        by='stop_population_station_dist').drop_duplicates(subset=['Day_x',
                                                             'ID_Fermata_x',
                                                             'Linea','Corsa'
                                                             ],
                                                             keep='first')

# Now df_filtered contains only the closest match 
final_df = df_filtered

final_df = final_df[['vehicle_or_asset_ID', 'Day_x', 'ID_Fermata_x', 'stop_name_x',
       'date_timestamp_MC', 'date_timestamp_UTC+_MC', 'timestamp_raw_MC',
       'Linea', 'Corsa', 'Direction', 'Numero fermata_x',
       'Fermata effettuata_x', 'Boarding_front_MC', 'Alighting_front_MC',
       'Boarding_mid_MC', 'Alighting_mid_MC', 'Boarding_back_MC',
       'Alighting_back_MC', 'Total_boarding_MC', 'Total_alighting_MC', 'VL_MC',
       'Traffic', 'People', 'Building', 'lat_MC', 'lon_MC', 'hour', 'stop_lat',
       'stop_lon', 'date_timestamp_GTT', 'Mezzo', 'VL_GTT',
       'Total_boarding_GTT', 'Total_alighting_GTT', 'Boarding_front_GTT',
       'Alighting_front_GTT', 'Saliti porta 2 ', 'Alighting_mid_GTT',
       'Boarding_back_GTT', 'Alighting_back_GTT', 'time_diff',
       'timestamp_dt_main', 'ID_Fermata_y', 'Next stop ID', 'Day_y',
       'stop_name_y', 'Numero fermata_y', 'Fermata effettuata_y', 'Minute',
       'occupants_count', 'WiFi_predicted_count', 'geometry',
       'value_pollution_PM10', 'valore_temp_aria', 'valore_prec', 'valore_hum',
       'valore_radi', 'valore_wind_speed', 'Num_zona_s', 'Desc_zona_', 'Femmine', 
       'Maschi', 'Total']].rename(columns={'Num_zona_s' : 'num_stat_zone_popu', 'Desc_zona_': 'desc_zone_popu', 'Femmine':'total_female_popu', 
       'Maschi':'total_male_popu', 'Total':'Total_popullation'})

final_df = final_df.sort_values(by=['date_timestamp_UTC+_MC']).reset_index(drop=True)
                                           

final_df['date_timestamp_UTC+_MC'] = final_df['date_timestamp_UTC+_MC'].apply(lambda a: datetime.strftime(a,"%Y-%m-%d %H:%M:%S.%f%z"))                                                    
final_df.to_excel("Merging datasets/6.Popullation_Meteo_Pollution_WiFi_GTT_and_Manual_count_June_2023.xlsx")

























